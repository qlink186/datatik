# README #.

### Database: ###

- File database terdapat pada folder sql
- import database dengan nama:
  -> nama database : dk-tpi2
  -> password database : (kosong)
- untuk mengubah konfigurasi database : app.js

### Menjalankan aplikasi: ###

- Pastikan node.js dan npm sudah terinstall di komputer.
- Setelah database berhasil diimport, buka cmd dari folder root projek (atau arahkan cmd pada folder root projek)
- Jalan kan : "npm install" untuk menginstall seluruh  dependencies yang terdapat pada package.json
- Setelah berhasil jalankan dengan "npm start"
- untuk mengakses website ketik : "localhost:3000"
- Untuk akses ke dashboard/admin panel, ketik "localhost:3000/adm" pada browser maka akan beralih ke form login, untuk login masukkan username "qlink186" dan password "enteraja" (lihat tabel d_users)